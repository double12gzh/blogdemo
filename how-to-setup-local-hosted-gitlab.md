> 说明： 本文是对[这篇](https://double12gzh.github.io/fragment/2023-10-13-local-install-gitlab/)文章的进一步说明

# 1. 前提

- gitlab/gitlab-runner 已前文运行成功，如下：

```bash
root@debian:~# docker ps
CONTAINER ID   IMAGE                         COMMAND                  CREATED      STATUS                 PORTS                                                                                                                                                              NAMES
732f805490dd   gitlab/gitlab-runner:latest   "/usr/bin/dumb-init …"   3 days ago   Up 54 minutes                                                                                                                                                                             gitlab-runner
abb8c68e7527   gitlab/gitlab-ce:latest       "/assets/wrapper"        3 days ago   Up 7 hours (healthy)   0.0.0.0:8929->8929/tcp, :::8929->8929/tcp, 0.0.0.0:8022->22/tcp, :::8022->22/tcp, 0.0.0.0:8080->80/tcp, :::8080->80/tcp, 0.0.0.0:8443->443/tcp, :::8443->443/tcp   gitlab
```

# 2. 创建代码库
在 group name 为 `personal` 的分组中创建名为 `blog` 的项目，即 `ssh://git@gitlab.debian.com:8022/personal/blog.git`

# 3. 配置变更
搜索本代码中 `zenghui` 关键字处的内容

# 4. 访问

`http://personal.gitlab.debian.com:8929/blog`

# 5. 其它
gitlab-runner 的配置

```bash
root@debian:~# cat GITLAB_HOME/gitlab-runner/config/config.toml
concurrent = 1
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

....

[[runners]]
  name = "Docker Runner"
  url = "http://gitlab.debian.com:8929/"
  id = 4
  token = "glrt-noQP5xVr6yyo5Xkk5rrF"
  token_obtained_at = 2023-10-13T16:36:40Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
  [runners.docker]
    tls_verify = false
    image = "docker:latest"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    network_mode = "host"
    shm_size = 0

[[runners]]
  name = "Docker Runner"
  url = "http://gitlab.debian.com:8929/"
  id = 5
  token = "vzi_WdmB6uXmB4yomcLm"
  token_obtained_at = 2023-10-13T16:42:01Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
  [runners.docker]
    tls_verify = false
    image = "ruby:2.7"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    network_mode = "host"
    shm_size = 0

[[runners]]
  name = "Docker Runner"
  url = "http://gitlab.debian.com:8929/"
  id = 6
  token = "4WRyWEdFgUngCGwpLsvP"
  token_obtained_at = 2023-10-14T05:39:25Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
  [runners.docker]
    tls_verify = false
    image = "ruby:2.7"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    network_mode = "host"
    shm_size = 0

[[runners]]
  name = "Docker Runner"
  url = "http://gitlab.debian.com:8929/"
  id = 7
  token = "_WAY_cMF-s6MkxKb4rrQ"
  token_obtained_at = 2023-10-14T07:30:13Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
  [runners.docker]
    tls_verify = false
    image = "ruby:2.7"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    network_mode = "host"
    shm_size = 0
...
```
